import React, { Component } from 'react';
import './Pager.css';

class Pager extends React.Component {
    constructor(props) {
        super(props);
        this.previous = this.previous.bind(this);
        this.next = this.next.bind(this);

    }

    previous() {
        this.props.previous();
    }

    next(event) {
        this.props.next();
    }

    render() {
        return (
            <div className={'pager'}>
                <button onClick={this.previous} disabled={this.props.disabledPrevious}>Previous</button>
                <button onClick={this.next} disabled={this.props.disabledNext}>Next</button>
            </div>
        )
    }
}

export default Pager;
