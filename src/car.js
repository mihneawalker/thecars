import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import './car.css'

class Car extends React.Component {
    constructor(props) {
        super(props);

        this.handleClick = this.handleClick.bind(this);

    }

    handleClick() {
        this.props.editCar(this.props.car.id);
    }

    capitalize(s) {
        return s.split(' ').map(word => word[0].toUpperCase() + word.slice(1)).join(' ') ;
    }

    render() {
        return (
          <div className={'car'} onClick={this.handleClick}>
              <h4>{this.capitalize(this.props.car.name)}</h4>
              <div className={'carDetails'}>
                  <div>{this.props.car.displacement} cc.</div>
                  <div>{this.props.car.horsepower} bhp</div>
                  <div>{this.props.car.mpg} mpg</div>
                  <div> Year: '{this.props.car.year}</div>
                  <div> Weight: {this.props.car.weight} lbs</div>
                  <div> Cylinders: {this.props.car.cylinders}</div>
                  <div> Acceleration: {this.props.car.acceleration}</div>
              </div>
          </div>
        );
    }
}

export default Car;
