import React, { Component } from 'react';
import './EditCar.css';

class EditCar extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            car: this.props.car,
            edited: false
        };
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.onCancel = this.onCancel.bind(this);

    }

    handleChange(event) {
        const target = event.target;
        const value = target.value;
        const name = target.name;
        this.setState({
            car: {
                ...this.state.car,
                [name]: value,
            }
        });
        this.setState({edited: true});

    }

    handleSubmit(event) {
        event.preventDefault();
        this.props.editedCar(this.state.car);
    }

    onCancel() {
        if (this.state.edited) {
            if (window.confirm('Fields edited. Are you sure you want to abandon changes?')) {
                this.props.onCancel()
            }
        } else {
            this.props.onCancel();
        }
    }

    render() {

        return (
            <form className={'formEdit'} onSubmit={this.handleSubmit}>
                <div className={'row'}>
                    <label>Name</label>
                    <input name="name" type="text" onChange={this.handleChange} value={this.state.car.name}/>
                </div>
                <div className={'row'}>
                    <label>Displacement</label>
                    <input name="displacement" type="text" onChange={this.handleChange} value={this.state.car.displacement}/>
                </div>
                <div className={'row'}>
                    <label>Horsepower</label>
                    <input name="horsepower" type="number" onChange={this.handleChange} value={this.state.car.horsepower}/>
                </div>
                <div className={'row'}>
                    <label>MPG</label>
                    <input name="mpg" type="number" onChange={this.handleChange} value={this.state.car.mpg}/>
                </div>
                <div className={'row'}>
                    <label>Year</label>
                    <input name="year" type="number" onChange={this.handleChange} value={this.state.car.year}/>
                </div>
                <div className={'row'}>
                    <label>Weight</label>
                    <input name="weight" type="number" onChange={this.handleChange} value={this.state.car.weight}/>
                </div>
                <div className={'row'}>
                    <label>Cylinders</label>
                    <input name="cylinders" type="number" onChange={this.handleChange} value={this.state.car.cylinders}/>
                </div>
                <div className={'row'}>
                    <label>Acceleration</label>
                    <input name="acceleration" type="number" onChange={this.handleChange} value={this.state.car.acceleration}/>
                </div>

                <div className={'editFooter'}>
                    <button type={'button'} onClick={this.onCancel}>Cancel</button>
                    <button type={'submit'}>Update</button>
                </div>
            </form>
        )
    }
}

export default EditCar;
