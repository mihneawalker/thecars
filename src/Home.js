import React, { Component } from 'react';
import CarsList from './CarsList';
import Pager from "./Pager";
import './home.css';

class Home extends React.Component{
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className={'home'}>
                <h2>Cars</h2>
                <CarsList cars={this.props.cars} editCar={this.props.editCar} />
                <Pager
                    previous={this.props.onPrevious}
                    next={this.props.onNext}
                    disabledPrevious={!this.props.previous}
                    disabledNext={!this.props.next}
                />
            </div>
        )
    }


}

export default Home;
