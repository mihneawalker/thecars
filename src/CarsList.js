import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import Car from './car';
import './CarsList.css';

class CarsList extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className={'carsList'}>
                {this.props.cars.map(car => {
                    return (
                        <Car car={car} key={car.id} editCar={this.props.editCar}/>
                    )
                }) }
            </div>
        )
    }
}

export default CarsList;
