import React, { Component } from 'react';
import { Router, Route, Switch } from 'react-router-dom';
import './App.css';
import history from './history';
import Home from './Home';
import EditCar from './EditCar';


class App extends Component {

    constructor(props) {
        super(props);
        this.state = {
            cars: [],
            meta: {},
        };
        this.onPrevious = this.onPrevious.bind(this);
        this.onNext= this.onNext.bind(this);
        this.onEditCar = this.onEditCar.bind(this);
        this.updateCar = this.updateCar.bind(this);
    }

    componentDidMount() {
        this.getData('/api/v1/car/?format=json');
    }

    getData(url) {
        fetch(`http://localhost/${url}`)
            .then(response => response.json())
            .then(data => {
                this.setState({cars: data.objects});
                this.setState({meta: data.meta});
            });
    }

    onPrevious() {
        if (this.state.meta.previous) {
            this.getData(this.state.meta.previous);
        }
    }

    onNext() {
        if (this.state.meta.next) {
            this.getData(this.state.meta.next);
        }
    }

    getCarById(id) {
        var numId = parseInt(id, 10);
        return this.state.cars.find(car => car.id === numId);
    }

    onEditCar(car) {
        history.push(`/edit/${car}`);
    }
    onCancel() {
        history.push('/');
    }

    updateCar(data) {
        history.push('/');
        fetch(`http://localhost${data.resource_uri}?format=json`, {
            method: 'PUT',
            body: JSON.stringify(data),
            mode: 'cors',
            headers: {
                "Content-Type": "application/json",
            }
        })
            .then(response => response.json())
            .then(response => {
                const idPosition = this.state.cars.findIndex(c => c.id === response.id);
                const localCars = [...this.state.cars];
                localCars[idPosition] = response;
                this.setState({
                    cars: localCars
                })
            })
            .catch(error => console.error(error));
    }

    render() {
        return (
        <div className="App">
            <Router history={history}>
                <Switch>
                    <Route exact path="/" render={ () => (
                        <Home
                            cars={this.state.cars}
                            previous={this.state.meta.previous}
                            next={this.state.meta.next}
                            onPrevious={this.onPrevious}
                            onNext={this.onNext}
                            editCar={this.onEditCar}
                        />
                    )}/>
                    <Route exact path="/edit/:id" render={ props => (
                        <EditCar
                            car={this.getCarById(props.match.params.id)}
                            onCancel={this.onCancel}
                            editedCar={this.updateCar}/>
                    )}/>
                </Switch>
            </Router>
        </div>
        );
  }
}

export default App;
